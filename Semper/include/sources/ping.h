#pragma once
void ping_destroy(void** spv);
double ping_update(void* spv);
void ping_reset(void* spv, void* ip);
void ping_init(void** spv, void* ip);
