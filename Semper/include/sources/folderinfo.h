#pragma once

void folderinfo_init(void** spv, void* ip);
void folderinfo_reset(void* spv, void* ip);
double folderinfo_update(void* spv);
void folderinfo_destroy(void** spv);
