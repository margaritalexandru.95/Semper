#pragma once
void string_source_init(void** pv, void* ip);
unsigned char* string_source_string(void* pv);
double string_source_update(void* pv);
void string_source_destroy(void** pv);
void string_source_reset(void* pv, void* ip);
